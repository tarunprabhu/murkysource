# Murky

Murky is a dark theme for GTK Source View.

## Installation

Copy the `gtksourceview-3.0` and `gtksourceview-4` directories to the user themes
directory. This is usually one of:

- `$HOME/.local/share`
- `$HOME/.themes`
- `$XDG_DATA_HOME`

To make the theme available to all users, copy the `gtksourceview-3.0` and 
`gtksourceview-4` directories to the system theme path. This is usually 
`/usr/share/themes` or `/usr/local/share/themes`.
